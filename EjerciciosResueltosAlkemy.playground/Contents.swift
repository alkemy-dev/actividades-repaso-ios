//TEMA 1 y TEMA 2

//***Ejercitación 1 Actividad de repaso en un Playground de Xcode.

//Crea una variable de tipo String llamada parcial1 y asígnale un texto con una nota entre 1 y 10 (sin decimales).
//
//Crea una variable llamada parcial2 y asígnale un texto con una nota entre 1 y 10, incluyendo un valor decimal.
//
//Convierte las variables del punto 1 y 2 en enteros. ¿Qué sucede con la nota de parcial2?
//
//Convierte las variables del punto 1 y 2 en otro tipo de dato de forma que no se pierda la precisión de las notas. ¿Hace alguna diferencia que el fraccional de parcial2 se defina usando punto o coma?
//
//Utiliza print para mostrar en consola un texto indicando las notas obtenidas; por ejemplo, si en el primer parcial se obtuvo 5 y en el segundo 8.3, el texto debe consistir en “Los resultados del parcial fueron 5 en el primero y 8.3 en el segundo”.

// 1.
var parcial1:String = "5"
//2.
var parcial2 = "6.0"
//3.
Int(parcial1)!
//No se puede convertir parcial2
//4.
Int(parcial1)!
Double(parcial2)!
// El fracional se debe definir con un ".", no se puede definir con ","
//5.
print("Los resultados del parcial fueron \(parcial1) en el primero y \(parcial2) en el segundo")

//***Ejercitación 2 en el Playground de Xcode

//El estado de una materia depende del valor del promedio:
//mayor o igual a 7 ➡️ Aprobada;
//mayor o igual a 4 y menor que 7 ➡️ Debe presentarse al final;
//menor a 4 ➡️ Reprobada;

//Teniendo en cuenta estos datos, lleva a cabo las siguientes tareas:

//1. Crea tres variables que corresponden a las notas de los parciales de una materia y en una cuarta variable calcula la nota final de la materia promediando dichos parciales.

//2. Crea una variable materiaAprobada que indique si la materia se aprobó.

//3. Crea una variable requiereFinal que indique si es necesario presentar el examen final.

//4. Utilizando las variables del punto 2 y 3, crea una variable materiaReprobada que indique si la materia fue reprobada.

//1.
var nota1 = 4
var nota2 = 8
var nota3 = 10
var notaFinal = (nota1 + nota2 + nota3)/3
//2
var materiaAprobada = notaFinal >= 7
//3
var requiereFinal = notaFinal >= 4 && notaFinal < 7
//4
var materiaReprobada = notaFinal < 4

//***Ejercitación 3 en el Playground de Xcode
//
//1. Crea una variable del tipo string llamada miEdadEnTexto y asígnale un texto con tu edad en números.
//
//2. Crea una variable opcional del tipo entero llamada miEdad y asígnale el resultado de convertir miEdadEnTexto a entero.
//
//3. Toma la variable miEdad creada anteriormente y muestra en consola un mensaje indicando tu edad, por ejemplo “Mi edad es 30 años”. Utiliza nil coalescing para mostrar 0 como edad por defecto en caso de que el valor sea nil.

//1.
var miEdadEnTexto: String = "30"
//2.
var miEdad: Int? = Int(miEdadEnTexto)
//3.
print("Mi edad es \(miEdad ?? 0)")

//TEMA 3. TIPO DE DATOS II

//***Ejercitación 1 en el Playground de Xcode

//1. Representa la fecha de hoy (día, mes, año) en una tupla de enteros.
//2. Actualiza la tupla del ejercicio anterior agregando nombres a los elementos de forma que pueda identificarse mejor cuál es el día, mes y año respectivamente.
//3. Utilizando la tupla anterior, muestra en consola un mensaje con la fecha completa, por ejemplo, “Hoy es 10 del mes 6 del año 2021”.

//1.
var fecha = (01, 09, 2021)
//2.
let fechaDeHoyConNombre = (dia:3, mes:09, año:2021)
//3.
print("Hoy es \(fechaDeHoyConNombre.dia) del mes \(fechaDeHoyConNombre.mes) del año \(fechaDeHoyConNombre.año) " )

//***Ejercitación 2 en el Playground de Xcode

//1. Define una constante que sea un rango con los números del 0 al 100 inclusive.
//2. Define una constante que sea un rango con todos los números hasta el 300 sin incluirlo.

//1.
let rango = 0...100
//2.
let rangoHasta300 = ..<300

//***Ejercitación 3 en el Playground de Xcode

//1. ¿Cuál es el problema en el siguiente código?: var ciudades: [Int] = ["Lima", "Buenos Aires","Rio de Janeiro"]
//a. La inicialización del arreglo: debe agregarse () al inicializar un arreglo.
//b. La inicialización del arreglo: no pueden agregarse valores iniciales al momento de crear la variable.
//c. Tipo de dato del arreglo: el tipo de dato no coincide con el tipo de los valores.
//d. La inicialización del arreglo: No debe especificarse el tipo de dato si se incluyen valores iniciales.
//2. Dado el siguiente arreglo: var frutas = ["Melon", "Pera", "Sandía", "Naranja", "Manzana"], escriba un rango para obtener: ["Pera", "Sandía", "Naranja"].
//3. Consulta la documentación de Apple y, con el arreglo de frutas del ejercicio anterior, elimina los primeros 2 elementos.

//1. = C
//2.
var frutas = ["Melon", "Pera", "Sandía", "Naranja", "Manzana"]
print (frutas[1...3])
//3.
frutas.remove(at: 0)
frutas.remove(at: 0)
print(frutas)

//***Ejercitación 4 en el Playground de Xcode

//Considera la siguiente clasificación y luego lleva a cabo las tareas propuestas a continuación.
//
//Lenguajes Modernos: Go, Kotlin, Swift, TypeScript
//Lenguajes Maduros: C#, Java, C++
//Lenguajes Viejos: C, Fortran, Cobol
//
//1. Declara cada uno de los tres conjuntos: lengModernos, lengMaduros, lengViejos.
//
//2. Escribe un conjunto lengTodos que incluya las tres categorías.
//
//3. Muestra un código de ejemplo para determinar si un lenguaje pertenece a una categoría dada.

//1.
let lengModernos: Set = ["Go", "Kotlin", "Swift", "TypeScript"]
let lengMaduros: Set = ["C#", "Java", "C++"]
let lengViejos: Set = ["C", "Fortran", "Cobol"]
//2.
let lengTodos: Set = [lengViejos,lengMaduros,lengModernos]
//3.
let lenguaje = "Kotlin"
for len in lengTodos{
    print (len.contains(lenguaje))
}

//***Ejercitación 5 en el Playground de Xcode

//1. Crea una variable de tipo diccionario que contenga las notas de los alumnos:
//| Nombre | Nota |
//|---------|------|
//| Alberto | 10 |
//| Ignacio | 4 |
//| Pedro | 7 |
//
//2. Tendrás una lista de ciudades y necesitarás una estructura para almacenar cuáles fueron visitadas y cuáles no. Escribe la declaración para almacenar dicha información.
//| Ciudad | Visitada |
//|---------|------|
//| CABA | Si |
//| Mar del Plata | No |
//| Santa fe | No |
//| Rosario | Si |
//
//3. Agrégale a la estructura anterior una ciudad que hayas visitado y una que te gustaría visitar.
//
//4. Luego de tus vacaciones, pudiste conocer esa ciudad que querías. Actualiza la estructura para indicar que ya la has visitado.

//1.
var notas: [String: Int] = [:]
notas["Alberto"] = 10
notas["Ignacio"] = 4
notas["Pedro"] = 7
print(notas)
//2.
var ciudades: [String : Bool] = [:]
ciudades["CABA"] = true
ciudades["Mar del Plata"] = false
ciudades["Santa fe"] = false
ciudades["Rosario"] = true
//3.
ciudades["Cordoba"] = true
ciudades["Ushaia"] = false
print(ciudades)
//4.
ciudades["Ushaia"] = true
print(ciudades)

//TEMA 4. CONTROL DE FLUJOS

//***Ejercitación 1 en el Playground de Xcode

//1. Declara una constante con un valor entero correspondiente a la edad de una persona, luego define un If que valide lo siguiente respecto de la edad:
//
//Es menor a 13 ➡️ imprima “Niño”
//Está entre 14 y 17 ➡️ imprima “Adolescente”
//Está entre 18 y 39 ➡️ imprima “Adulto jóven”
//Está entre 40 y 49 ➡️ imprima “Adulto medio”
//Mayor a 50 ➡️ imprima “Adulto maduro”
//
//2. Teniendo tres número enteros en constantes a, b y c, imprime dichos números ordenados de mayor a menor. Asume que los números son diferentes entre sí.

//1.
let edadPersona = 40

if edadPersona < 13 {
    print("Niño")
} else if edadPersona >= 14 || edadPersona <= 17 {
    print("Adolescente")
} else if edadPersona >= 18 || edadPersona <= 39 {
    print("Adulto jóven")
} else if edadPersona >= 40 || edadPersona <= 49 {
    print("Adulto medio")
} else {
    print("Adulto maduro")
}
//2.
let a = 5
let b = 2
let c = 9

if a < b && a < c {
    if b < c {
        print(a, b, c)
    } else {
        print(a, c, b)
    }
} else if b < a && b < c {
    if a < c {
        print(b, a, c)
    } else {
        print(b, c, a)
    }
} else if c < a && c < b {
    if a < b {
        print (c, a, b)
    } else {
        print (c, b, a)
    }
}
